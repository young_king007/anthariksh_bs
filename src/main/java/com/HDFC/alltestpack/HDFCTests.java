package com.HDFC.alltestpack;

import java.util.Map;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.HDFC.library.functions.MethodDef;
import com.HDFC.library.teststeps.HDFCTestSteps;

public class HDFCTests extends HDFCMethods{
	
	/**
	 * Compatibility Test Cases
	 * @param brow
	 * @param data
	 * @param ctx
	 * @throws InterruptedException
	 */
	@Test(description = "HDFC - Layout Tests", dataProvider = "TestDataParallel")
	public static void HDFCCompatabilityFlow(Map<String, String> brow, Map<String, String> data, ITestContext ctx)
			throws InterruptedException {
		HDFCTestSteps steps = new HDFCTestSteps();
		String page_name="HDFCCompatabilityFlow";
		steps.sampleHDFCHdfcCompatibility(page_name);
		try {
			System.out.println("*****Quit Driver*****");
//		MethodDef.quitDriver();
	} catch(Exception e) {
		System.out.println("******Ran in Browser stack*******");
	}
	}
	
}

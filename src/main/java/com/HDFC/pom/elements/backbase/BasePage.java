package com.HDFC.pom.elements.backbase;

import org.openqa.selenium.By;

import com.HDFC.framework.Locators;
import com.HDFC.library.functions.CommonDef;

public class BasePage {
		
	public static By locateus() {
		return CommonDef.locatorValue(Locators.XPATH, "//a[contains(text(), 'Locate us')]");
	}

}

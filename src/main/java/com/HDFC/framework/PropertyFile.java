package com.HDFC.framework;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertyFile {

	public static String sspath = "";

	public static String getSspath() {
		System.out.println("Report Path--->"+System.getProperty("SSPath"));
		if(System.getProperty("SSPath")!=null)
			setSspath(System.getProperty("SSPath"));
		else 
			setSspath(ProjectConfig.getPropertyValue("SSPath"));
		return sspath;
	}

	public static void setSspath(String sspath) {
		PropertyFile.sspath = sspath;
	}
	
	public static String getAccountNumber(String scenarioID){
		Properties prop = new Properties();
		InputStream input;
		try {
			input = new FileInputStream("src/main/resources/AccountInfo.properties");
			prop.load(input);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop.getProperty(scenarioID);
	}
	
}

package com.HDFC.framework;

import java.io.FileOutputStream;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TestReport {

	public static void name() {
		Map<String,Map<String,Integer>> result = ExReporter.result;
		@SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Result");
		int rowCount = 0;
		Row row = sheet.createRow(++rowCount);
		int columnCount = 0;
		Cell cell = row.createCell(++columnCount);
		cell.setCellValue("Category");
		cell = row.createCell(++columnCount);
		cell.setCellValue("Pass");
		cell = row.createCell(++columnCount);
		cell.setCellValue("Fail");
		cell = row.createCell(++columnCount);
		cell.setCellValue("Other");
		for (Entry<String, Map<String, Integer>> res : result.entrySet()) {	
			row = sheet.createRow(++rowCount);
			columnCount = 0;
			cell = row.createCell(++columnCount);
			cell.setCellValue(res.getKey());
			cell = row.createCell(++columnCount);
			cell.setCellValue(res.getValue().get("pass"));
			cell = row.createCell(++columnCount);
			cell.setCellValue(res.getValue().get("fail"));
			cell = row.createCell(++columnCount);
			cell.setCellValue(res.getValue().get("other"));
		} 
		row = sheet.createRow(++rowCount);
		columnCount = 0;
		cell = row.createCell(++columnCount);
		cell.setCellValue("Total");
		cell = row.createCell(++columnCount);
		cell.setCellFormula("SUM(C3:C"+(rowCount)+")");
		cell = row.createCell(++columnCount);
		cell.setCellFormula("SUM(D3:D"+(rowCount)+")");
		cell = row.createCell(++columnCount);
		cell.setCellFormula("SUM(E3:E"+(rowCount)+")");
			FileOutputStream outputStream;
			try {
				outputStream = new FileOutputStream("Report.xlsx");
				workbook.write(outputStream);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
		
	}
}

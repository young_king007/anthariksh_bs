package com.HDFC.Driver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.HDFC.framework.ConfigProvider;
import com.HDFC.framework.ExReporter;
import com.HDFC.framework.ProjectConfig;
import com.HDFC.framework.TestData;
import com.relevantcodes.extentreports.LogStatus;

public class BrowserStackDriver {

	public static final String USERNAME = ProjectConfig.getPropertyValue("BSUserName");
	public static final String AUTOMATE_KEY = ProjectConfig.getPropertyValue("BSKey");
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	static WebDriver remoteDriver = null;

	public WebDriver getBSDriver() {
		remoteDriver = getRemoteDriver();
		return remoteDriver;
	}
	public static WebDriver getRemoteDriver() {

		DesiredCapabilities desiredCap = new DesiredCapabilities();
		desiredCap.setCapability("browserstack.local", true);
		desiredCap.setCapability("browserstack.autoWait", 0);
		desiredCap.setCapability("build", ProjectConfig.getPropertyValue("buildNumber"));
		desiredCap.setCapability("project", "SCB");
		desiredCap.setCapability("browserstack.localIdentifier", "Test123");
		desiredCap.setCapability("acceptSslCerts", "true");
		String browsr = ConfigProvider.getConfig("Browser");
		String os = ConfigProvider.getConfig("OS");
		String versn = ConfigProvider.getConfig("Version");
		String testName=ConfigProvider.getConfig("Testname");
		String os_version=ConfigProvider.getConfig("OSversion");

		try {
			if(os.toUpperCase().contains("MAC")) {
				os="OS X";
			}
			switch (browsr.toUpperCase()) {
			case "IE":
				desiredCap.setCapability("browser", browsr);
				desiredCap.setCapability("browser_version", versn);
				desiredCap.setCapability("os", os);
				desiredCap.setCapability("os_version", os_version);
				desiredCap.setCapability("resolution", "1024x768");
				desiredCap.setCapability("name", testName);
				break;
			case "FIREFOX":
				System.out.println("Firefox Browser");
				desiredCap.setCapability("os", os);
				desiredCap.setCapability("browser", "Firefox");
				desiredCap.setCapability("browser_version", "68.0");
				desiredCap.setCapability("os", "Windows");
				desiredCap.setCapability("os_version", "7");
				desiredCap.setCapability("resolution", "1024x768");
				desiredCap.setCapability("name", "Bstack-[Java] Sample Test");
				break;
			case "CHROME":
				desiredCap.setCapability("browser", "Chrome");
				desiredCap.setCapability("browser_version", "76.0");
				desiredCap.setCapability("os", "Windows");
				desiredCap.setCapability("os_version", "7");
				desiredCap.setCapability("resolution", "1024x768");
				desiredCap.setCapability("name", "Bstack-[Java] Sample Test");
				break;
			case "SAFARI":
				System.out.println("Safari Browser");
				desiredCap.setCapability("browser", "Safari");
				desiredCap.setCapability("browser_version", "12.0");
				desiredCap.setCapability("os", "OS X");
				desiredCap.setCapability("os_version", "Mojave");
				desiredCap.setCapability("resolution", "1024x768");
				desiredCap.setCapability("name", "Bstack-[Java] Sample Test");
				break;
			case "Android":
				//System.out.println("Android Browser");
				desiredCap.setCapability("platform", "ANDROID");
				//desiredCap.setCapability("browserName", "Chrome");
				desiredCap.setCapability("device", TestData.getConfig("devicename"));
				//desiredCap.setCapability("platform ","win");
				desiredCap.setCapability("os", "Android");
				desiredCap.setCapability("browser", "Android");
				break;
			case "desktop":
				//System.out.println(" Browser");
				desiredCap.setCapability("os", os);
				desiredCap.setCapability("browser", TestData.getConfig("BrowserName"));
				desiredCap.setCapability("browser_version", versn);
				break;
			case "ios":
				//System.out.println("Iphone Browser");
				desiredCap.setCapability("platform", "IOS");
				desiredCap.setCapability("device", TestData.getConfig("devicename"));
				desiredCap.setCapability("os", "IOS");
				//desiredCap.setCapability("device", "iPhone 7");
				//desiredCap.setCapability("realMobile", "true");
				desiredCap.setCapability("browser", "iPhone");
				//desiredCap.setCapability("browserstack.appium_version", "1.6.3");
				//desiredCap.setCapability("realMobile", true);
				break;
			default:
				desiredCap = DesiredCapabilities.firefox();
				break;
			}
			System.out.println("Before Remote");
			remoteDriver = new RemoteWebDriver(new URL(URL), desiredCap);
			System.out.println("After Remote"+(new URL(URL)));
			ExReporter.log(LogStatus.INFO, "Browser Initiated successfully");
			remoteDriver.get(ProjectConfig.getPropertyValue("url"));
			remoteDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		} catch (Exception e) {
			try {
				Thread.sleep(2000);
				remoteDriver = new RemoteWebDriver(new URL(URL), desiredCap);
				//remoteDriver.manage().window().maximize();
				remoteDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} catch (Exception e1) {
				System.out.println(e1.getMessage());
				ExReporter.log(LogStatus.FATAL, "Browser Initiation Failed :" + e1.getMessage());
				
			}
		}
		return remoteDriver;
	}
}
package com.HDFC.GalenFramework;

import java.util.List;

import com.HDFC.framework.ConfigProvider;
import com.HDFC.framework.ProjectConfig;
import com.HDFC.framework.TestData;
import com.galenframework.reports.GalenTestInfo;


public class GalenMethods {

	/**
	 * Galen Specification Test
	 * 
	 * @param specname
	 */
	public static void galenspec(String specname, String fileName, String groupname) {
	
		try {
			List<GalenTestInfo> tests = GalenLayout.initiateTest();
			@SuppressWarnings("unused")
			String filter = "";
			//Change the filename based on config Provider Browser value
			
			if(ConfigProvider.getConfig("OS").equalsIgnoreCase("Device"))  //  Android ||  ConfigProvider.getConfig("OS").equals("ios"))
				filter = "";
			else 
				filter = getBrowserSectionFilter();
			fileName=fileName.toLowerCase();
			System.out.println("Spec Name ::" +specname);
			String Desc="layout-check-for-" + fileName + "page-on-" + getBrowserSectionFilter() + "-view";
			GalenLayout.testLayout("layouttest/" + specname + ".gspec", fileName,Desc, tests);

			
			//GalenLayout.closeTestGroup(tests, "target/galen-html-reports/test");		
			GalenLayout.closeTestGroup(fileName+"_"+ConfigProvider.getConfig("Browser")+"_"+ConfigProvider.getConfig("Version")+"_"+getBrowserSectionFilter(),tests, System.getProperty("Report_Path"),Desc);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Validates Browser Size and returns the appropriate section name
	 * 
	 * @return Section Name
	 */
	public static String getBrowserSectionFilter() {
		if (!TestData.getConfig("width").equals("-")) {
			if (Integer.parseInt(TestData.getConfig("width")) > Integer
					.parseInt(ProjectConfig.getPropertyValue("tabwid"))) {
				return ProjectConfig.getPropertyValue("desktopdim");
			} else if (Integer.parseInt(TestData.getConfig("width")) < Integer
					.parseInt(ProjectConfig.getPropertyValue("desktopwid"))
					&& Integer.parseInt(TestData.getConfig("width")) > Integer
							.parseInt(ProjectConfig.getPropertyValue("mobilewid"))) {
				return ProjectConfig.getPropertyValue("tabdim");
			} else {
				return ProjectConfig.getPropertyValue("mobiledim");
			}
		} else {
			return ProjectConfig.getPropertyValue("desktopdim");
		}
	}
}

package com.HDFC.GalenFramework;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.HDFC.Driver.DriverFactory;
import com.HDFC.framework.ConfigProvider;
import com.HDFC.framework.ExReporter;
import com.galenframework.api.Galen;
import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;
import com.galenframework.reports.model.LayoutReport;
import com.relevantcodes.extentreports.LogStatus;



public class GalenLayout {

	static WebDriver  browser=null;
	public static List<GalenTestInfo> initiateTest() {
		List<GalenTestInfo> tests = new LinkedList<GalenTestInfo>();
		return tests; //
	}

	
	public static void testLayout(String pageSpec, String fileName, String testDescription, List<GalenTestInfo> tests) {

		if(ConfigProvider.getConfig("Platform").equalsIgnoreCase("Browser")||ConfigProvider.getConfig("Platform").equalsIgnoreCase("BrowserStack")) {
		 browser = DriverFactory.getCurrentDriver();
			}
		String sectionFilter;
		//System.out.println(ConfigProvider.getConfig("Platform"));
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Browser")
				|| (ConfigProvider.getConfig("Platform").equalsIgnoreCase("BrowserStack")
						&& !ConfigProvider.getConfig("OS").equalsIgnoreCase("Device"))) {
			sectionFilter = GalenMethods.getBrowserSectionFilter();
			fileName = fileName + " " + sectionFilter;
			ExReporter.log(LogStatus.INFO, "Validation webpage Layout on section :" + sectionFilter);
			ExReporter.assignCatogory(sectionFilter + " Viewport");
		} else {
			if (ConfigProvider.getConfig("OS").equalsIgnoreCase("Device")
					|| (ConfigProvider.getConfig("Platform").equalsIgnoreCase("BrowserStack")
							&& ConfigProvider.getConfig("Version").equalsIgnoreCase("Mobile")))
				sectionFilter = "mobile";
				sectionFilter = "tablet";
			ExReporter.assignCatogory(sectionFilter);
		}
		
		try {
			LayoutReport layoutReport = Galen.checkLayout(browser, pageSpec, Arrays.asList(sectionFilter));	
			GalenTestInfo test = GalenTestInfo.fromString(testDescription);
			test.getReport().layout(layoutReport, "check layout on " + sectionFilter + " device");
			tests.add(test);
			if (layoutReport.errors() > 0) {
				ExReporter.log(LogStatus.FAIL, "Layout is not correct");
			} else {
				ExReporter.log(LogStatus.PASS, "The Layout tests passed");
			}
			//ExReporter.log(LogStatus.INFO, "<font color=\"blue\">>" + "<a href=" + "\"" + userDirectory + fileName
			//		+ ".html" + "\"" + " target=\"_blank\"" + ">" + "Result File" + "</a></i></font><br/>");
			// closeTestGroup(tests, "target/galen-html-reports/test");

		} catch (Exception e) {
			ExReporter.log(LogStatus.FATAL, "UNABLE TO TEST LAYOUT - Error Message : " + e.getMessage());
		}
	}

	private static String buildInput(String loc) {
		if (loc.contains("target")) {
			return loc.substring(0, loc.length() - 7);
		} else {
			return loc;
		}
	}

	public static void closeTestGroup(String fileName,List<GalenTestInfo> tests, String path,String Desc) {
		try {
			
			// Unused date and time so removed
			
//			Calendar cal = Calendar.getInstance();
//			Format sdf = new SimpleDateFormat("yyyy-MM-dd");
//			Format sdf1 = new SimpleDateFormat("HH:mm:ss");
//			String dt = sdf.format(cal.getTime());
//			String time = sdf1.format(cal.getTime());

			//String report = path + "/" + dt + "/" + time;  File.pathSeparator returns :
			
			String testDesc="1-";
			testDesc+=Desc;
			
			String report = path + "/" + fileName;
			String path1= path.replace(".", "");	
			
			new HtmlReportBuilder().build(tests, report);
			final String RELATIVE_LOC = Paths.get("").toAbsolutePath().toString();
			final String PROJECT_LOC = buildInput(RELATIVE_LOC);
			
			String userDirectory = PROJECT_LOC +path1+ "/"+fileName+"/"+testDesc;	
				
			ExReporter.log(LogStatus.INFO, "<font color=\"blue\">>" + "<a href=" + userDirectory 
					+ ".html" + ">" + "Result File" + "</a></font><br/>");
			// closeTestGroup(tests, "target/galen-html-reports/test");
			
		} catch (Exception e) {
			ExReporter.log(LogStatus.WARNING, "Unable to group layout tests" + e.getLocalizedMessage());
		}
	}
}

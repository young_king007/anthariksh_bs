package com.HDFC.library.teststeps;


import com.HDFC.Driver.DriverFactory;
import com.HDFC.GalenFramework.GalenMethods;
import com.HDFC.PageObject.HdfcPage;
import com.HDFC.framework.ConfigProvider;
import com.HDFC.framework.ExReporter;
import com.HDFC.framework.ProjectConfig;
import com.HDFC.framework.TestData;
import com.HDFC.library.functions.MethodDef;
import com.relevantcodes.extentreports.LogStatus;

public class HDFCTestSteps {
	public static String Browser = ConfigProvider.getConfig("Browser").toUpperCase();

	public HDFCTestSteps() {
		initReport();
		logDetails();
		DriverFactory.driverInit();
	}
	public static void initReport() {
		try {
			String testName = ConfigProvider.getConfig("Testname") + "-" + TestData.getConfig("DataBinding");
			String desc = "";
			if (ProjectConfig.getPropertyValue("versionspecific").equals("true")
					|| ConfigProvider.getConfig("Platform").contains("BrowserStack"))
				desc = ConfigProvider.getConfig("Browser") + "-" + ConfigProvider.getConfig("Version");
			else
				desc = ConfigProvider.getConfig("Browser");
			ExReporter.initTest(testName, desc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// System.out.println(ProjectConfig.getPropertyValue("url"));
	private void logDetails() {
		try {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Browser"))
				ExReporter.log(LogStatus.INFO, "URL: " + ProjectConfig.getPropertyValue("url"));
			else
				ExReporter.log(LogStatus.INFO, "URL: " + ProjectConfig.getPropertyValue("murl"));
			ExReporter.log(LogStatus.INFO, "OS: " + ConfigProvider.getConfig("OS"));
			ExReporter.assignCatogory(ConfigProvider.getConfig("OS"));
			ExReporter.assignCatogory(ConfigProvider.getConfig("Testname"));
			// ExReporter.assignCatogory(ConfigProvider.getConfig("Testname")+"
			// => "+ConfigProvider.getConfig("EPIC"));
			if (ConfigProvider.getConfig("EPIC") != null) {
				ExReporter.assignCatogory(ConfigProvider.getConfig("EPIC"));
			}
			// ExReporter.assignCatogory(ConfigProvider.getConfig("Testname"));
			/**
			 * if
			 * (ProjectConfig.getPropertyValue("versionspecific").equals("true")
			 * 
			 * || ConfigProvider.getConfig("Platform").contains("BrowserStack"))
			 * {
			 * 
			 * ExReporter.log(LogStatus.INFO,
			 * 
			 * "Browser: " + ConfigProvider.getConfig("Browser") + "-" +
			 * ConfigProvider.getConfig("Version"));
			 * 
			 * ExReporter.assignCatogory(ConfigProvider.getConfig("Browser") +
			 * "-" + ConfigProvider.getConfig("Version"));
			 * 
			 * } else {
			 * 
			 * ExReporter.log(LogStatus.INFO, "Browser: " +
			 * ConfigProvider.getConfig("Browser"));
			 * 
			 * ExReporter.assignCatogory(ConfigProvider.getConfig("Browser"));
			 * 
			 * }
			 */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public void sampleHDFCHdfcCompatibility(String page_name) {
		MethodDef.launchHdfcApplication();
		GalenMethods.galenspec("hdfcPage", "hdfcPage", "hdfcPage");
		HdfcPage.navLocateUs();
		System.out.println("*******Locate page appears**********");
		GalenMethods.galenspec("hdfcLocatePage", "hdfcLocatePage", "hdfcLocatePage");
		//CommonDef.checkBrokenLinks();
//		GalenMethods.galenspec("hdfcProdPage", "hdfcProdPage", "hdfcProdPage");
	}
		
		
}
